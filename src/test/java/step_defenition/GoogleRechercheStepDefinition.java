package step_defenition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;


import static modules.Hooks.driver;
import static page_objects.GoogleRecherchePage.searchFor;

public class GoogleRechercheStepDefinition {

    @Given("I open google search page")
    public void iOpenGoogleSearchPage() {
        //System.setProperty("webdriver.chrome.driver","src/test/resources/drivers/chromedriver.exe" );
        //driver = new ChromeDriver();
        driver.get("https://www.google.fr/");
        driver.findElement(By.id("zV9nZe")).click(); // cookie problem
    }

    @When("I tape the word {string}")
    public void iTapeTheWord(String arg0){
      /*  driver.findElement(By.name("q")).clear();
        driver.findElement(By.name("q")).sendKeys(arg0);
        driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[3]/center/input[1]")).click(); */

        searchFor(arg0);

    }


    @Then("Search results display the word {string}")
    public void searchResultsDisplayTheWord(String arg0) {
        driver.getTitle();
        driver.quit();
    }
}
