package modules;


import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import page_objects.GoogleRecherchePage;


// Initier et détruire le test
public class Hooks  {  // configurer les actions avant et après le test
    public static WebDriver driver;
    private GoogleRecherchePage googleRecherchePage = new GoogleRecherchePage();

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","src/test/ressources/drivers/chromedriver.exe" );
        driver = new ChromeDriver();
        PageFactory.initElements(driver, googleRecherchePage);
    }

    @After
    public void tearDown(){
        driver.quit();

    }

}
