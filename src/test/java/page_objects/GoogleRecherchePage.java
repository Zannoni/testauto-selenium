package page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GoogleRecherchePage{

    /*  stocker toutes les options de page de Google - Page Object Model */
    /* driver.findElement(By.name("q")).clear();
        driver.findElement(By.name("q")).sendKeys(arg0);
        driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[3]/center/input[1]")).click(); */

    @FindBy(name = "q")
    private static WebElement searchBar;

    @FindBy(xpath = "/html/body/div[1]/div[3]/form/div[1]/div[1]/div[3]/center/input[1]")
    private static WebElement searchButton;

    public static void searchFor(String keyword){
        searchBar.clear();
        searchBar.sendKeys(keyword);
        searchButton.click();

    }
}
