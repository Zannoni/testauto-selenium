package utils;

import com.sun.deploy.cache.BaseLocalApplicationProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.HashMap;

// configurer les navigateurs
public class BrowserFactory {
    static WebDriver factoryDriver;
    private static HashMap<String, String> configurationMap = PropertiesFile.read("src/test/resources/environment/config.properties");
    static boolean headless = Boolean.parseBoolean(configurationMap.get("isHeadless"));
    private static String webBrowerType = configurationMap.get("browser"); ;


    public static WebDriver getFactoryDriver(){
        String driversPath = "src/test/ressources/drivers";
        switch (webBrowerType) {
            case "chrom":
            System.setProperty("webdriver.chrome.driver", driversPath + "chromedriver.exe");// récupérer dans Hooks.java
            ChromeOptions options = new ChromeOptions();
            options.setHeadless(headless);
            factoryDriver = new ChromeDriver(options);
            break;
            case "ie":
                System.setProperty("webdriver.ie.driver", driversPath + "IEDriverServer.exe");
                factoryDriver = new InternetExplorerDriver();
                break;
            case "firefox":
                System.setProperty("webdriver.gecko.driver", driversPath + "geckodriver.exe");
                factoryDriver = new FirefoxDriver();
                break;
            default:
                System.setProperty("webdriver.chrome.driver", driversPath + "chromedriver.exe");
                factoryDriver = new ChromeDriver();
                break;
        }

        return factoryDriver;
    }
}