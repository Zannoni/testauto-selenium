@google_recherche
Feature: Google Recherche
  As a user to test google search function

  Background:pre-requisites
    Given I open google search page

  @smoke
  Scenario: search with a key word
    When I tape the word "publicis sapient"
    Then Search results display the word "publicis sapient"

  @ignore
  Scenario Template: <title>
    When I tape the word "search_word"
    Then Search results display the word "search_result"
    Examples:
      | title                                   | search_word        | search_result |
      | Passing scenario using Gherkin keywords | cucumber           | cucumber      |
      | Failing scenario using Gherkin keywords | publicis sapient   | razorfish     |

